/* eslint-disable linebreak-style */
/* eslint-disable require-jsdoc */
/* eslint-disable linebreak-style */
const Space = require('../models/Space');

async function createSpace(
    idUser, location, title, availability, services, img, property, spaceType) {
  try {
    console.log(location);
    const newSpace = new Space();
    newSpace.idUser = idUser;
    newSpace.location = location;
    newSpace.title = title;
    newSpace.availability = availability;
    newSpace.services = services;
    newSpace.property = property;
    newSpace.spaceType = spaceType;

    if (img) {
      newSpace.pictures = img;
    }

    const space = await newSpace.save();
    return space;
  } catch (error) {
    throw (err);
  }
}

async function getSpaceById(id) {
  try {
    const newSpace = await Space.findById(id)
        .populate('idUser');
    return newSpace;
  } catch (error) {
    throw (err);
  }
}

async function getSpaceByIdUser(idUser) {
  try {
    const newSpace = await Space.find({'idUser': idUser});
    return newSpace;
  } catch (error) {
    throw (err);
  }
}

async function getAllSpaces() {
  try {
    const Spaces = await Space.find()
        .populate('idUser');
    return Spaces;
  } catch (error) {
    throw (err);
  }
}

async function updateSpaceById(id, options) {
  try {
    const updatedFields = {};
    if (options.idUser) {
      updatedFields.idUser = options.idUser;
    }
    if (options.location) {
      updatedFields.location = options.nalocationme;
    }
    if (options.pictures) {
      updatedFields.pictures = options.pictures;
    }
    if (options.title) {
      updatedFields.title = options.title;
    }
    if (options.availability) {
      updatedFields.availability = options.availability;
    }
    if (options.services) {
      updatedFields.services = options.services;
    }
    if (options.property) {
      updatedFields.property = options.property;
    }
    if (options.spaceType) {
      updatedFields.spaceType = options.spaceType;
    }

    // console.log(updatedFields);
    const newSpace = await Space.findByIdAndUpdate(
        id, updatedFields, {new: true});
    // console.log(newSpace);
    return newSpace;
  } catch (error) {
    throw (error);
  }
}

async function deleteSpaceById(id) {
  try {
    const newSpace = await Space.findByIdAndDelete(id);
    return newSpace;
  } catch (error) {
    throw (error);
  }
}

async function overwriteSpaceById(id, fields) {
  try {
    const SpaceDoc = await getSpaceById(id);
    SpaceDoc.overwrite(fields);
    await SpaceDoc.validate();
    return await SpaceDoc.save();
  } catch (err) {
    throw err;
  }
}


module.exports = {
  createSpace,
  getSpaceByIdUser,
  getSpaceById,
  getAllSpaces,
  deleteSpaceById,
  updateSpaceById,
  overwriteSpaceById,
};
