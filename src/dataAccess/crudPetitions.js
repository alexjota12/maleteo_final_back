/* eslint-disable linebreak-style */
/* eslint-disable require-jsdoc */
/* eslint-disable linebreak-style */
const Petition = require('../models/Petition');

async function createPetition(idUser, idGuardian, entryDate, departureDate) {
  try {
    const newPetition = new Petition();
    newPetition.idUser = idUser;
    newPetition.idGuardian = idGuardian;
    newPetition.entryDate = entryDate;
    newPetition.departureDate = departureDate;
    newPetition.state = 'pending';

    const ptt = await newPetition.save();

    return ptt;
  } catch (error) {
    throw (err);
  }
}

async function getPetitionByIdUser(idUser) {
  try {
    const newPetitions = await Petition.find({'idUser': idUser});
    return newPetitions;
  } catch (error) {
    throw (err);
  }
}

async function getPetitionByIdGuardian(idGuardian) {
  try {
    const newPetitions = await Petition.find({'idGuardian': idGuardian})
        .populate('idUser');
    return newPetitions;
  } catch (error) {
    throw (err);
  }
}

async function getPetitionById(id) {
  try {
    const newPetition = await Petition.findById(id);
    return newPetition;
  } catch (error) {
    throw (err);
  }
}

async function getAllPetitions() {
  try {
    const Petitions = await Petition.find();
    return Petitions;
  } catch (error) {
    throw (err);
  }
}

async function updatePetitionById(id, options) {
  try {
    const updatedFields = {};
    if (options.idUser) {
      updatedFields.idUser = options.idUser;
    }
    if (options.idGuardian) {
      updatedFields.idGuardian = options.idGuardian;
    }
    if (options.entryDate) {
      updatedFields.entryDate = options.entryDate;
    }
    if (options.departureDate) {
      updatedFields.departureDate = options.departureDate;
    }
    if (options.state) {
      updatedFields.state = options.state;
    }

    // console.log(updatedFields);
    const newPetition = await Petition.findByIdAndUpdate(
        id, updatedFields, {new: true});
    // console.log(newPetition);
    return newPetition;
  } catch (error) {
    throw (error);
  }
}

async function deletePetitionById(id) {
  try {
    const newPetition = await Petition.findByIdAndDelete(id);
    return newPetition;
  } catch (error) {
    throw (error);
  }
}

async function overwritePetitionById(id, fields) {
  try {
    const PetitionDoc = await searchPetitionById(id);
    PetitionDoc.overwrite(fields);
    await PetitionDoc.validate();


    return await PetitionDoc.save();
  } catch (err) {
    throw err;
  }
}


module.exports = {
  createPetition,
  getPetitionByIdUser,
  getPetitionByIdGuardian,
  getPetitionById,
  getAllPetitions,
  deletePetitionById,
  updatePetitionById,
  overwritePetitionById,
};
