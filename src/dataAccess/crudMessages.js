/* eslint-disable linebreak-style */
/* eslint-disable require-jsdoc */
/* eslint-disable linebreak-style */
const Message = require('../models/Message');

async function createMessage(idUserSend, idUserReceive, sentDate, text) {
  try {
    const newMessage = new Message();
    newMessage.idUserSend = idUserSend;
    newMessage.idUserReceive = idUserReceive;
    newMessage.sentDate = sentDate;
    newMessage.text = text;

    const msg = await newMessage.save();

    return msg;
  } catch (error) {
    throw (err);
  }
}

async function getMessageByIdUserSend(idUser) {
  try {
    const newMessages = await Message.find({'idUserSend': idUser});
    return newMessages;
  } catch (error) {
    throw (err);
  }
}

async function getMessageByIdUserReceive(idUser) {
  try {
    const newMessages = await Message.find({'idUserReceive': idUser});
    return newMessages;
  } catch (error) {
    throw (err);
  }
}

async function getMessageById(id) {
  try {
    const newMessage = await Message.findById(id);
    return newMessage;
  } catch (error) {
    throw (err);
  }
}

async function getAllMessages() {
  try {
    const Messages = await Message.find()
        .populate('idUserSend')
        .populate('idUserReceive')
        .sort( {'sentDate': 1} );
    return Messages;
  } catch (error) {
    throw (err);
  }
}

async function updateMessageById(id, options) {
  try {
    const updatedFields = {};
    if (options.idUserSend) {
      updatedFields.idUserSend = options.idUserSend;
    }
    if (options.idUserReceive) {
      updatedFields.idUserReceive = options.idUserReceive;
    }
    // if (options.sentDate) {
    //   updatedFields.sentDate = options.sentDate;
    // }
    if (options.text) {
      updatedFields.text = options.passtextword;
    }

    // console.log(updatedFields);
    const newMessage = await Message.findByIdAndUpdate(
        id, updatedFields, {new: true});
    // console.log(newMessage);
    return newMessage;
  } catch (error) {
    throw (error);
  }
}

async function deleteMessageById(id) {
  try {
    const newMessage = await Message.findByIdAndDelete(id);
    return newMessage;
  } catch (error) {
    throw (error);
  }
}

async function overwriteMessageById(id, fields) {
  try {
    const MessageDoc = await searchMessageById(id);
    MessageDoc.overwrite(fields);
    await MessageDoc.validate();


    return await MessageDoc.save();
  } catch (err) {
    throw err;
  }
}


module.exports = {
  createMessage,
  getMessageByIdUserSend,
  getMessageByIdUserReceive,
  getMessageById,
  getAllMessages,
  deleteMessageById,
  updateMessageById,
  overwriteMessageById,
};
