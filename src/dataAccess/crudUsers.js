/* eslint-disable linebreak-style */
/* eslint-disable max-len */
/* eslint-disable require-jsdoc */
const User = require('../models/User');

async function createUser(name, surname, email, password, img) {
  try {
    const newUser = new User();
    newUser.name = name;
    newUser.surname = surname;
    newUser.email = email;
    newUser.password = password;
    newUser.guardian = false;
    if (img) {
      newUser.img = img;
    }

    // newUser.img = buffer;

    const usr = await newUser.save();
    return usr;
  } catch (error) {
    throw (err);
  }
}

async function getUserByEmail(email) {
  try {
    const newUser = await User.findOne({'email': email});
    return newUser;
  } catch (error) {
    throw (err);
  }
}

async function getUserById(id) {
  try {
    const newUser = await User.findById(id);
    return newUser;
  } catch (error) {
    throw (err);
  }
}

async function getAllUsers() {
  try {
    const users = await User.find();
    return users;
  } catch (error) {
    throw (err);
  }
}

async function updateUserById(id, options) {
  try {
    // console.log('Options ' + options);
    const updatedFields = {};
    if (options.surname) {
      updatedFields.surname = options.surname;
    }
    if (options.name) {
      updatedFields.name = options.name;
    }
    if (options.email) {
      updatedFields.email = options.email;
    }
    if (options.birthdate) {
      updatedFields.birthdate = options.birthdate;
    }
    if (options.password) {
      updatedFields.password = options.password;
    }
    if (options.guardian) {
      updatedFields.guardian = options.guardian;
    }
    if (options.advertising) {
      updatedFields.advertising = options.advertising;
    }
    if (options.idSpace) {
      updatedFields.idSpace = options.idSpace;
    }
    if (options.idPetition) {
      updatedFields.idPetition = options.idPetition;
    }
    if (options.idMessage) {
      updatedFields.idMessage = options.idMessage;
    }
    if (options.img && options.img != 'undefined') {
      updatedFields.img = options.img;
    }
    // console.log(updatedFields);
    const newUser = await User.findByIdAndUpdate(id, updatedFields, {new: true});
    // console.log(newUser);
    return newUser;
  } catch (error) {
    throw (error);
  }
}

async function deleteUserById(id) {
  try {
    const newUser = await User.findByIdAndDelete(id);
    return newUser;
  } catch (error) {
    throw (error);
  }
}

async function overwriteUserById(id, fields) {
  try {
    const userDoc = await searchUserById(id);
    userDoc.overwrite(fields);
    await userDoc.validate();


    return await userDoc.save();
  } catch (err) {
    throw err;
  }
}


module.exports = {
  createUser,
  getUserByEmail,
  getUserById,
  getAllUsers,
  deleteUserById,
  updateUserById,
  overwriteUserById,
};
