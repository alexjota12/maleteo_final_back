/* eslint-disable linebreak-style */
const PetitionMiddleware = require('../dataAccess/crudPetitions');

const createPetition = async (req, res, next) => {
  try {
    console.log('petitionController: ', req.body);
    const newPetition = await PetitionMiddleware.createPetition(
        req.body.idUser,
        req.body.idGuardian,
        req.body.entryDate,
        req.body.departureDate,
        'pending',
    );
    res.json({newPetition: newPetition});
  } catch (err) {
    next(err);
  }
};

const listPetitions = async (req, res, next) => {
  try {
    const list = await PetitionMiddleware.getAllPetitions();
    res.json({Petitions: list});
  } catch (err) {
    next(err);
  }
};

const searchPetitionById = async (req, res, next) => {
  try {
    const usr = await PetitionMiddleware.getPetitionById(req.params.id);
    res.json({Petitions: [usr]});
  } catch (err) {
    next(err);
  }
};

const searchPetitionByIdGuardian = async (req, res, next) => {
  try {
    const usr = await PetitionMiddleware.getPetitionByIdGuardian(
        req.params.idGuardian);
    res.json({Petitions: usr});
  } catch (err) {
    next(err);
  }
};

const searchPetitionByIdUser = async (req, res, next) => {
  try {
    const usr = await PetitionMiddleware.getPetitionByIdUser(
        req.params.idUser);
    res.json({Petitions: [usr]});
  } catch (err) {
    next(err);
  }
};

const deletePetition = async (req, res, next) => {
  try {
    const usr = await PetitionMiddleware.deletePetitionById(req.params.id);
    res.json({Petitions: [usr]});
  } catch (err) {
    next(err);
  }
};

const updatePetition = async (req, res, next) => {
  try {
    const usr = await PetitionMiddleware.updatePetitionById(
        req.params.id, req.body);
    res.json({Petitions: [usr]});
  } catch (err) {
    next(err);
  }
};

const overwritePetition = async (req, res, next) => {
  try {
    const usr = await PetitionMiddleware.overwritePetitionById(
        req.params.id, req.body);
    res.json({Petitions: [usr]});
  } catch (err) {
    next(err);
  }
};

module.exports = {
  createPetition,
  listPetitions,
  searchPetitionById,
  deletePetition,
  updatePetition,
  overwritePetition,
  searchPetitionByIdGuardian,
  searchPetitionByIdUser,
};
