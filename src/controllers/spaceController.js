/* eslint-disable linebreak-style */
const spaceMiddleware = require('../dataAccess/crudSpaces');

const createSpace = async (req, res, next) => {
  try {
    console.log('spaceController: ', req.body);
    const newSpace = await spaceMiddleware.createSpace(
        req.body.idUser,
        {
          lat: req.body.lat,
          lng: req.body.lng,
          name: req.body.name,
        },
        req.body.title,
        req.body.availability,
        req.body.services,
        req.fileImg,
        req.body.property,
        req.body.spaceType,
    );
    res.json({newSpace: newSpace});
  } catch (err) {
    next(err);
  }
};

const listSpaces = async (req, res, next) => {
  try {
    const list = await spaceMiddleware.getAllSpaces();
    res.json({Spaces: list});
  } catch (err) {
    next(err);
  }
};

const searchSpaceById = async (req, res, next) => {
  try {
    const spc = await spaceMiddleware.getSpaceById(req.params.id);
    res.json({Space: spc});
  } catch (err) {
    next(err);
  }
};

const searchSpaceByIdUser = async (req, res, next) => {
  try {
    const spc = await spaceMiddleware.getSpaceByIdUser(req.params.idUser);
    res.json({Spaces: spc});
  } catch (err) {
    next(err);
  }
};

const deleteSpace = async (req, res, next) => {
  try {
    const spc = await spaceMiddleware.deleteSpaceById(req.params.id);
    res.json({Space: spc});
  } catch (err) {
    next(err);
  }
};

const updateSpace = async (req, res, next) => {
  req.body.pictures = req.fileImg;
  try {
    const spc = await spaceMiddleware.updateSpaceById(req.params.id, req.body);
    res.json({Space: spc});
  } catch (err) {
    next(err);
  }
};

const overwriteSpace = async (req, res, next) => {
  try {
    const spc = await spaceMiddleware.overwriteSpaceById(
        req.params.id, req.body);
    res.json({Space: spc});
  } catch (err) {
    next(err);
  }
};

module.exports = {
  createSpace,
  listSpaces,
  searchSpaceById,
  deleteSpace,
  updateSpace,
  overwriteSpace,
  searchSpaceByIdUser,
};
