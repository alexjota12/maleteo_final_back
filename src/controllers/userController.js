/* eslint-disable linebreak-style */
const userMiddleware = require('../dataAccess/crudUsers');

const createUser = async (req, res, next) => {
  try {
    console.log(req.body);
    const newUser = await userMiddleware.createUser(
        req.body.name,
        req.body.surname,
        req.body.email,
        req.body.password,
        req.fileImg,
    );

    res.json({newUser: newUser});
  } catch (err) {
    next(err);
  }
};

const listUsers = async (req, res, next) => {
  try {
    const list = await userMiddleware.getAllUsers();
    res.json({users: list});
  } catch (err) {
    next(err);
  }
};

const searchUserById = async (req, res, next) => {
  try {
    const usr = await userMiddleware.getUserById(req.params.id);
    res.json({users: [usr]});
  } catch (err) {
    next(err);
  }
};

const searchUserByEmail = async (req, res, next) => {
  try {
    const usr = await userMiddleware.getUserByEmail(req.params.email);
    res.json({users: [usr]});
  } catch (err) {
    next(err);
  }
};

const deleteUser = async (req, res, next) => {
  try {
    const usr = await userMiddleware.deleteUserById(req.params.id);
    res.json({users: [usr]});
  } catch (err) {
    next(err);
  }
};

const updateUser = async (req, res, next) => {
  // console.log(req.fileImg);
  req.body.img = req.fileImg;
  try {
    // console.log('Body ' + req.body.surname);
    const usr = await userMiddleware.updateUserById(req.params.id, req.body);
    res.json({users: [usr]});
  } catch (err) {
    next(err);
  }
};

const overwriteUser = async (req, res, next) => {
  try {
    const usr = await userMiddleware.overwriteUserById(req.params.id, req.body);
    res.json({users: [usr]});
  } catch (err) {
    next(err);
  }
};

module.exports = {
  createUser,
  listUsers,
  searchUserById,
  deleteUser,
  updateUser,
  overwriteUser,
  searchUserByEmail,
};
