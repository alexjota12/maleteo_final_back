/* eslint-disable linebreak-style */
/* eslint-disable max-len */
// const User = require('../models/User');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/mainConfig');

// PASSPORT
const userLocalRegister = passport.authenticate('local-register', {
  session: false,
});

const userLocalLogin = passport.authenticate('local-login', {session: false});


const userGoogleRegister = passport.authenticate('google-register', {
  scope: ['profile', 'email'],
});

const userGoogleRegisterCallback = passport.authenticate('google-register', {
  session: false,
});

const userGoogleLogin = passport.authenticate('google-login', {
  scope: ['profile'],
});

const userGoogleLoginCallback = passport.authenticate('google-login', {
  session: false,
});

const userFacebook = passport.authenticate('facebook', {
  session: false,
  scope: ['email'],
});

// const userFacebook2 = passport.authenticate('facebook', {
//   successRedirect: '/success',
//   failureRedirect: '/fail',
// });

const checkAuth = function(req, res) {
  // console.log(req);
  if (req.user === null) {
    res.json({error: 'UserNotExist', message: 'El email no está registrado'});
  } else {
    console.log(req.authInfo);
    if (req.authInfo.error ) {
      res.json({error: req.authInfo.name, message: req.authInfo.error});
    } else {
      /**
     * TODO: No pasar la contraseña en el token. Problema seguridad.
     */
      const credentials = {email: req.user.email};
      if (req.user.password) {
        credentials.password = req.user.password;
      } else if (req.user.idFacebook) {
        credentials.socialId = req.user.idFacebook;
      } else {
        credentials.idGoogle = req.user.idGoogle;
      }

      jwt.sign(credentials, config.app.jwt_secret, {expiresIn: '1200000s'}, function(
          err,
          token,
      ) {
        res.json({token: token, id: req.user._id});
      });
    }
  }
};

const checkAuthSocial = function(req, res) {
  // console.log(req);
  if (req.user === null) {
    res.json({error: 'UserNotExist', message: 'El email no está registrado'});
  } else {
    // console.log(req.authInfo);
    if (req.authInfo.error) {
      res.redirect('http://localhost:3000/login/social/error' + '/' + req.authInfo.error);
      // res.json({error: req.authInfo.name, message: req.authInfo.message});
    } else {
      /**
     * TODO: No pasar la contraseña en el token. Problema seguridad.
     */
      const credentials = {email: req.user.email};
      if (req.user.password) {
        credentials.password = req.user.password;
      } else if (req.user.idFacebook) {
        credentials.socialId = req.user.idFacebook;
      } else {
        credentials.idGoogle = req.user.idGoogle;
      }

      jwt.sign(credentials, config.app.jwt_secret, {expiresIn: '1200000s'}, function(
          err,
          token,
      ) {
        res.redirect('http://localhost:3000/login/social/' + token + '/' + req.user._id);
        // res.json({token: token, id: req.user._id});
      });
    }
  }
};


// AUTHORITATION

const verifyToken = async function(req, res, next) {
  try {
    console.log(req.headers);
    if (req.headers.authorization) {
      const token = req.headers.authorization.split(' ')[1];
      if (await jwt.verify(token, config.app.jwt_secret)) {
        next();
      } else {
        res.sendStatus(401);
      }
    } else {
      res.sendStatus(401);
    }
  } catch (err) {
    next(err);
  }
};

module.exports = {
  userLocalRegister,
  userLocalLogin,
  userGoogleRegister,
  userGoogleRegisterCallback,
  userGoogleLogin,
  userGoogleLoginCallback,
  userFacebook,
  // userFacebook2,
  checkAuth,
  verifyToken,
  checkAuthSocial,
};

