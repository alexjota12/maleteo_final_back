/* eslint-disable linebreak-style */
const cloudinary = require('cloudinary').v2;
const streamifier = require('streamifier');
const multer = require('multer');
// const path = require('path');

const storage = multer.memoryStorage();

const cloudinaryConfig = {
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
};

cloudinary.config(cloudinaryConfig);

const upload = multer({storage: storage});

const uploadFile = function(imageName) {
  return upload.single(imageName);
};

const uploadMultipleFiles = function(arrayImages) {
  return upload.array(arrayImages);
};

const uploadToCloudinary = function(req, res, next) {
  console.log(req.file, req.files, req.body);
  if (!req.file) {
    return next();
  }
  const uploadStream = cloudinary.uploader.upload_stream(
      {
        folder: 'FINAL/AVATARS',
      },
      function(err, savedImage) {
        req.fileImg = savedImage.url;
        return next();
      },
  );

  streamifier.createReadStream(req.file.buffer).pipe(uploadStream);
};

const uploadToCloudinaryPictures = function(req, res, next) {
  if (!req.files) {
    return next();
  }
  const uploadStream = cloudinary.uploader.upload_stream(
      {
        folder: 'FINAL/PICTURES',
      },
      function(err, savedImage) {
        req.fileImg = savedImage.url;
        return next();
      },
  );
  streamifier.createReadStream(req.file.buffer).pipe(uploadStream);
};

module.exports = {
  uploadFile,
  uploadToCloudinary,
  uploadToCloudinaryPictures,
  uploadMultipleFiles,
};
