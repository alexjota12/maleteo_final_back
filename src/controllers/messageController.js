/* eslint-disable linebreak-style */
/* eslint-disable max-len */
const messageMiddleware = require('../dataAccess/crudMessages');

const createMessage = async (req, res, next) => {
  try {
    const newMessage = await messageMiddleware.createMessage(
        req.body.idUserSend,
        req.body.idUserReceive,
        req.body.sentDate,
        req.body.text,
    );
    res.json({newMessage: newMessage});
  } catch (err) {
    next(err);
  }
};

const listMessages = async (req, res, next) => {
  try {
    const list = await messageMiddleware.getAllMessages();
    res.json({Messages: list});
  } catch (err) {
    next(err);
  }
};

const searchMessageById = async (req, res, next) => {
  try {
    const msg = await messageMiddleware.getMessageById(req.params.id);
    res.json({Messages: [msg]});
  } catch (err) {
    next(err);
  }
};

const searchMessageByIdUserSend = async (req, res, next) => {
  try {
    const msg = await messageMiddleware.getMessageByIdUserSend(req.params.id);
    res.json({Messages: [msg]});
  } catch (err) {
    next(err);
  }
};

const searchMessageByIdUserReceive = async (req, res, next) => {
  try {
    const msg = await messageMiddleware.getMessageByIdUserReceive(req.params.id);
    res.json({Messages: [msg]});
  } catch (err) {
    next(err);
  }
};

const deleteMessage = async (req, res, next) => {
  try {
    const msg = await messageMiddleware.deleteMessageById(req.params.id);
    res.json({Messages: [msg]});
  } catch (err) {
    next(err);
  }
};

const updateMessage = async (req, res, next) => {
  try {
    const msg = await messageMiddleware.updateMessageById(req.params.id, req.body);
    res.json({Messages: [msg]});
  } catch (err) {
    next(err);
  }
};

const overwriteMessage = async (req, res, next) => {
  try {
    const msg = await messageMiddleware.overwriteMessageById(
        req.params.id, req.body);
    res.json({Messages: [msg]});
  } catch (err) {
    next(err);
  }
};

module.exports = {
  createMessage,
  listMessages,
  searchMessageById,
  searchMessageByIdUserSend,
  searchMessageByIdUserReceive,
  deleteMessage,
  updateMessage,
  overwriteMessage,
};
