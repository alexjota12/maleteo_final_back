/* eslint-disable linebreak-style */
module.exports = {
  app: {
    port: process.env.EXPRESS_PORT || 3030,
    morgan: process.env.MORGAN || 'dev',
    db_uri: process.env.DB_URI,
    jwt_secret: process.env.JWT_SECRET,
    google_api_key: process.env.GOOGLE_API_KEY,
    google_secret_key: process.env.GOOGLE_SECRET_KEY,
    google_callback_register_url: process.env.GOOGLE_CALLBACK_REGISTER_URL,
    google_callback_login_url: process.env.GOOGLE_CALLBACK_LOGIN_URL,
    facebook_client_id: process.env.FACEBOOK_CLIENT_ID,
    facebook_client_secret: process.env.FACEBOOK_CLIENT_SECRET,
    facebook_callback_url: process.env.FACEBOOK_CALLBAK_URL,
  },
};
