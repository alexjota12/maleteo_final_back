/* eslint-disable linebreak-style */
const express = require('express');
const logger = require('winston');

const config = require('./config/mainConfig');
const loaders = require('./loaders/mainLoader');

/** Punto de entrada
*/
function boostrapServer() {
  const app = express();
  
  const server = app.listen(config.app.port);
  server.on('error', onError);
  server.on('listening', () => {
    loaders(app);
    logger.info('Server running on port ' + config.app.port);
  });
}

/** Manejador de error
 * @param {String} err
 */
function onError(err) {
  console.log(err.code);
  switch (err.code) {
    case 'EACCES':
      logger.error(config.app.port + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      logger.error(config.app.port + ' is already in use');
      process.exit(1);
      break;
    default:
      logger.error(err);
  }
}

boostrapServer();
