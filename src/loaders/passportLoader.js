/* eslint-disable linebreak-style */
/* eslint-disable max-len */
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const userMiddleware = require('../dataAccess/crudUsers');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const bcrypt = require('bcrypt');
const User = require('../models/User');
const config = require('../config/mainConfig');
const FacebookStrategy = require('passport-facebook').Strategy;

// const cloudinary = require('cloudinary').v2;
// const streamifier = require('streamifier');
// const multer = require('multer');

// const storage = multer.memoryStorage();

// const cloudinaryConfig = {
//   cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
//   api_key: process.env.CLOUDINARY_API_KEY,
//   api_secret: process.env.CLOUDINARY_API_SECRET,
// };

// cloudinary.config(cloudinaryConfig);

// const upload = multer({storage: storage});
// const uploadFile = function(imageName) {
//   return upload.single(imageName);
// };

// const uploadStream = cloudinary.uploader.upload_stream(
//     {
//       folder: 'FINAL/AVATARS',
//     },
//     function(err, savedImage) {
//       req.fileImg = savedImage.url;
//       return next();
//     },
// );

const passportLoader = function(app) {
  passport.use(
      'local-register',
      new LocalStrategy(
          {
            passReqToCallback: true,
            usernameField: 'email',
          },
          async function(req, email, password, done) {
            // console.log('en passport local-register: ');
            try {
              const user = await userMiddleware.getUserByEmail(email);
              if (user === null) {
                console.log(req.body);

                const newUser = new User();
                newUser.name = req.body.name;
                newUser.surname = req.body.surname;
                newUser.email = req.body.email;
                newUser.password = req.body.password;
                newUser.advertising = req.body.ads;
                newUser.birthdate = req.body.birthdate;
                newUser.guardian = false;
                await newUser.save();
                return done(null, newUser);
              } else {
                const error = {};
                error.error = 'El email ya está registrado';
                error.name = 'UserExist';
                error.status = 204;
                // console.log(error);
                return done(null, user, error);
              }
            } catch (err) {
              return done(err, false);
            }
          },
      ),
  );

  passport.use(
      'local-login',
      new LocalStrategy(
          {
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true,
          },
          async function(req, email, password, done) {
            try {
              // console.log('en passport local-login: ');
              const user = await userMiddleware.getUserByEmail(email);
              if (user === null) {
                console.log('login - user error');
                const error = {};
                error.error = 'El email no está registrado';
                error.name = 'UserNotExist';
                error.status = 204;
                // console.log(error);
                return done(null, error, error);
              }

              if (await bcrypt.compare(password, user.password)) {
                console.log('login - correct');
                return done(null, user);
              } else {
                const error = {};
                error.error = 'El email o la contraseña son incorrectos';
                error.name = 'InvalidCredentials';
                error.status = 204;
                // console.log(error);
                return done(null, error, error);
              }
            } catch (error) {
              return done(error);
            }
          },
      ),
  );

  passport.use(
      'google-register',
      new GoogleStrategy(
          {
            clientID: config.app.google_api_key,
            clientSecret: config.app.google_secret_key,
            callbackURL: config.app.google_callback_register_url,
          },
          async function(accessToken, refreshToken, profile, done) {
            try {
              const user = await userMiddleware.getUserByEmail(profile.emails[0].value);
              if (user === null) {
                const newUser = new User();
                newUser.name = profile.name.givenName;
                newUser.surname = profile.name.familyName;
                newUser.email = profile.emails[0].value;
                newUser.idGoogle = profile.id;
                newUser.advertising = true;
                newUser.guardian = false;
                await newUser.save();
                return done(null, newUser);
              } else {
                const error = {};
                error.error = 'El email ya está registrado';
                error.name = 'UserExist';
                error.status = 204;
                // console.log(error);
                return done(null, user, error);
              }
            } catch (err) {
              console.log(err);
              return done(null, false);
            }
          },
      ),
  );

  passport.use(
      'google-login',
      new GoogleStrategy(
          {
            clientID: config.app.google_api_key,
            clientSecret: config.app.google_secret_key,
            callbackURL: config.app.google_callback_login_url,
          },
          async function(accessToken, refreshToken, profile, done) {
            try {
              const user = await User.findOne({idGoogle: profile.id});
              if (user === null) {
                throw new Error('Unauthorized');
              }

              return done(null, user);
            } catch (err) {
              console.log(err);
              return done(null, false);
            }
          },
      ),
  );

  passport.use(
      'facebook',
      new FacebookStrategy({
        clientID: config.app.facebook_client_id,
        clientSecret: config.app.facebook_client_secret,
        callbackURL: 'http://localhost:3030/login/facebook/callback',
        profileFields: [
          'id',
          'first_name',
          'last_name',
          'email',
          'gender',
          'picture.type(large)',
        ],
      },
      function(accessToken, refreshToken, profile, done) {
        console.log(profile);
        User.findOne({idFacebook: profile.id}, (err, user)=>{
          if (err) {
            return done(err);
          }
          if (user) {
            return done(null, user);
          } else {
            const user2 = userMiddleware.getUserByEmail(profile._json.email);
            if (user2 === null) {
              const newUser = new User();
              newUser.idFacebook = profile.id;
              newUser.name = profile._json.first_name;
              newUser.surname = profile._json.last_name;
              newUser.email = profile._json.email;
              newUser.advertising = true;
              newUser.guardian = false;

              newUser.save((err) => {
                if (err) throw err;
                return done(null, newUser);
              });
            } else {
              const error = {};
              error.error = 'El email ya está registrado';
              error.name = 'UserExist';
              error.status = 204;
              // console.log(error);
              return done(null, user, error);
            }
          }
        });
      },
      ));

  passport.serializeUser(function(user, done) {
    console.log(user);
    done(null, user);
  });

  passport.deserializeUser(async function(id, done) {
    done(null, null);
  });

  app.use(passport.initialize());
};

module.exports = passportLoader;
// passport.use(
//     'google-register',
//     new GoogleStrategy(
//         {
//           clientID: '562971353367-vs8d953160s9d8mc0fllvbrhfl93kuqa.apps.googleusercontent.com',
//           clientSecret: 'k5pAtI1IH7l3sjEHNRQzp8L4',
//           callbackURL: 'http://localhost:3000/register/google/callback',
//         },
//         async function(accessToken, refreshToken, profile, done) {
//           try {
//             const newUser = new User();
//             newUser.name = profile.name.givenName + ' ' + profile.name.familyName;
//             newUser.email = profile.emails[0].value;
//             newUser.socialId = profile.id;
//             await newUser.save();
//             return done(null, newUser);
//           } catch (err) {
//             console.log(err);
//             return done(null, false);
//           }
//         },
//     ),
// );

// passport.use(
//     'google-login',
//     new GoogleStrategy(
//         {
//           clientID: '562971353367-vs8d953160s9d8mc0fllvbrhfl93kuqa.apps.googleusercontent.com',
//           clientSecret: 'k5pAtI1IH7l3sjEHNRQzp8L4',
//           callbackURL: 'http://localhost:3000/login/google/callback',
//         },
//         async function(accessToken, refreshToken, profile, done) {
//           try {
//             const user = await User.findOne({socialId: profile.id});
//             if (user === null) {
//               throw new Error('Unauthorized');
//             }

//             return done(null, user);
//           } catch (err) {
//             console.log(err);
//             return done(null, false);
//           }
//         },
//     ),
// );
