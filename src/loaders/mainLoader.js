/* eslint-disable linebreak-style */
const logger = require('winston');
const loggerLoader = require('./loggerLoader');
const expressLoader = require('./expressLoader');
const mongooseLoader = require('./mongooseLoader');
const passportLoader = require('./passportLoader');

const loaders = async function(app) {
  loggerLoader(app);
  logger.info('Logger initializated!');

  await mongooseLoader();
  logger.info('DB initializated!');

  passportLoader(app);
  logger.info('Passport initializated!');

  expressLoader(app);
  logger.info('Express initializated!');
};

module.exports = loaders;
