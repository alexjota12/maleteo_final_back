/* eslint-disable linebreak-style */
const bodyParser = require('body-parser');
const logger = require('winston');
const cors = require('cors');

const mainRouter = require('../routes/mainRouter');

const expressLoader = function(app) {
  app.get('/status', (req, res, next) => {
    res.sendStatus(200);
  });
  app.head('/status', (req, res, next) => {
    res.sendStatus(200);
  });

  // app.use(bodyParser.urlencoded({extended: false}));
  app.use(bodyParser.urlencoded({extended: false}));
  app.use(bodyParser.json());

  app.use(cors());
  // app.use(function(req, res, next) {
  //   res.header('Access-Control-Allow-Origin', '*');
  //   res.header('Access-Control-Allow-Headers',
  //       'Origin, X-Requested-With, Content-Type, Accept');
  //   next();
  // });

  app.use(mainRouter);

  app.use(function(req, res, next) {
    const err = new Error('Not Found');
    err.name = 'NotFound';
    err.status = 404;
    next(err);
  });

  app.use(function(err, req, res, next) {
    switch (err.name) {
      case 'NotFound':
        err['status'] = 404;
        break;
      case 'Unauthorized':
        err['status'] = 401;
        break;
      // case '':
      //   err[] = 200;
      //   break;
      default:
        err['status'] = 500;
    }
    logger.error(err);
    res.status(err.status).json({errors: [err.name]});
  });
};


module.exports = expressLoader;
