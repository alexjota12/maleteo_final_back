/* eslint-disable linebreak-style */
const config = require('../config/mainConfig');
const logger = require('winston');
const mongoose = require('mongoose');

const mongooseLoader = async () => {
  try {
    await mongoose.connect(
        config.app.db_uri, {useNewUrlParser: true, useUnifiedTopology: true});
  } catch (err) {
    logger.error(err);
    throw err;
  }
};

module.exports = mongooseLoader;
