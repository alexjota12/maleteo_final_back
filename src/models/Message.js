/* eslint-disable linebreak-style */

/**
 * TODO: Creo que al igual que las peticiones, habría que añadir un campo
 * en User, que haga referencia a los mensajes del usuario.
 * Ponerlo en común, y si no lo vemos claro,
 * lo hablamos con Roberto y que nos ayude.
 * TODO: Otra idea, es que en vez de ser la colección de mensajes,
 * que sea de conversaciones. Que tendría como campos los id de los 2 usuarios,
 * y los mensajes, que sería un array de documentos con campos tipo fecha,
 * contenido, y remitente.
 * TODO: He puesto idUserSend e idUserReceive. También se puede dejar
 * idUser e idGuardian, y añadir otro campo que indique si el mensaje
 * lo envía el user o el guardian
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const MessageSchema = new Schema({
  // 'idMessage': {
  //   type: String,
  //   required: true,
  // },
  'idUserSend': {
    type: mongoose.Types.ObjectId, ref: 'User',
    required: true,
  },
  'idUserReceive': {
    type: mongoose.Types.ObjectId, ref: 'User',
    required: true,
  },
  'sentDate': {
    type: Date,
    required: true,
  },
  'text': {
    type: String,
    required: true,
  },
},
{
  timestamps: true,
});

const Message = mongoose.model('Message', MessageSchema);
module.exports = Message;
