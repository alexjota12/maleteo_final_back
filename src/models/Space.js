/* eslint-disable linebreak-style */

/**
 * TODO: Tipo de dato de location (Number o String)
 * Tipo de dato de pictures (¿referencia a otra DB o colección?)
 * Tipo de dato de availability (¿Fecha? Dudo al ser un rango)
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SpaceSchema = new Schema({
  // 'idSpace': {
  //   type: String,
  //   required: true,
  // },
  'idUser': {
    type: mongoose.Types.ObjectId, ref: 'User',
    required: true,
  },
  'location': {
    type: Object,
    // required: true,
  },
  'pictures': {
    type: String,
  },
  'title': {
    type: String,
    required: true,
  },
  'availability': [{
    type: Date,
    // required: true,
  }],
  'services': [{
    type: String,
  }],
  'property': {
    type: String,
  },
  'spaceType': {
    type: String,
  },
},
{
  timestamps: true,
});

const Space = mongoose.model('Space', SpaceSchema);
module.exports = Space;
