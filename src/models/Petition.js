/* eslint-disable linebreak-style */

/**
 * TODO: Se me ocurre que quizá las peticiones sean campos
 * de la colección User. Ponerlo en común, y si no lo vemos claro,
 * lo hablamos con Roberto y que nos ayude.
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PetitionSchema = new Schema({
  // 'idPetition': {
  //   type: String,
  //   required: true,
  // },
  'idUser': {
    type: mongoose.Types.ObjectId, ref: 'User',
    required: true,
  },
  'idGuardian': {
    type: mongoose.Types.ObjectId, ref: 'User',
    required: true,
  },
  'entryDate': {
    type: Date,
    required: true,
  },
  'departureDate': {
    type: Date,
    required: true,
  },
  'state': {
    type: String,
    required: true,
  },
},
{
  timestamps: true,
});

const Petition = mongoose.model('Petition', PetitionSchema);
module.exports = Petition;
