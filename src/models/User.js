/* eslint-disable linebreak-style */

/**
 * TODO: Decidir si la encripatación la realizamos aquí con
 *  el UserSchema.pre('save',...) o en el controlador.
 */

const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  // TODO: El id lo pone mongo directamente o queremos poner uno a parte?
  // Valorar si necesitamos imagen de perfil
  // 'idUser': {
  //   type: String,
  //   required: true,
  // },
  'guardian': {
    type: Boolean,
  },
  'email': {
    type: String,
    required: true,
  },
  'name': {
    type: String,
    required: true,
  },
  'surname': {
    type: String,
    required: true,
  },
  'password': {
    type: String,
    // required: true,
  },
  'idGoogle': {
    type: String,
  },
  'idFacebook': {
    type: String,
  },
  'advertising': {
    type: Boolean,
  },
  'idSpace': [{
    type: mongoose.Types.ObjectId, ref: 'Space',
  }],
  'idPetition': [{
    type: mongoose.Types.ObjectId, ref: 'Petition',
  }],
  'idMessage': [{
    type: mongoose.Types.ObjectId, ref: 'Message',
  }],
  'img': String,
  'birthdate': {
    type: Date,
  },
},
{
  timestamps: true,
});


userSchema.pre('save', async function(next) {
  try {
    if (!this.password) {
      return next();
    }
    this.password = await bcrypt.hash(this.password, 10);
    next();
  } catch (err) {
    next(err);
  }
});

const User = mongoose.model('User', userSchema);
module.exports = User;
