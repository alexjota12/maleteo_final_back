/* eslint-disable linebreak-style */
/* eslint-disable new-cap */

const express = require('express');
const router = express.Router();
const fileController = require('../controllers/fileController');
const userController = require('../controllers/userController');
const spaceController = require('../controllers/spaceController');
const petitionController = require('../controllers/petitionController');
const messageController = require('../controllers/messageController');
const passportController = require('../controllers/passportController');

/**
 * Passport
 */
router
    .route('/register')
    .post(fileController.uploadFile('img-avatar'),
        fileController.uploadToCloudinary,
        passportController.userLocalRegister,
        passportController.checkAuth);
router
    .route('/login')
    .post(passportController.userLocalLogin, passportController.checkAuth);

router
    .route('/register/google')
    .get(passportController.userGoogleRegister);

router
    .route('/register/google/callback')
    .get(passportController.userGoogleRegisterCallback,
        passportController.checkAuthSocial);

router
    .route('/login/google')
    .get(passportController.userGoogleLogin);

router
    .route('/login/google/callback')
    .get(passportController.userGoogleLoginCallback,
        passportController.checkAuthSocial);

router
    .route('/login/facebook')
    .get(passportController.userFacebook);

router
    .route('/login/facebook/callback')
    .get(passportController.userFacebook,
        passportController.checkAuthSocial);

router.get('/logout', function(req, res) {
  req.logout();
  res.json({msg: 'logout success'});
});

/**
 * User routes
 */
router
    .route('/user')
    .post(passportController.verifyToken,
        fileController.uploadFile('img-avatar'),
        fileController.uploadToCloudinary, userController.createUser)
    .get(passportController.verifyToken,
        passportController.verifyToken, userController.listUsers);

router
    .route('/user/email/:email')
    .get(userController.searchUserByEmail);

router
    .route('/user/:id')
    .get(passportController.verifyToken,userController.searchUserById)
    .delete(passportController.verifyToken,userController.deleteUser)
    .patch(passportController.verifyToken, fileController.uploadFile('file'),
        fileController.uploadToCloudinary, userController.updateUser)
    .put(passportController.verifyToken,userController.overwriteUser);


/**
 * Space routes
 */
router
    .route('/space')
    .post(passportController.verifyToken, fileController.uploadFile('file'),
        fileController.uploadToCloudinary, spaceController.createSpace)
    .get(passportController.verifyToken, spaceController.listSpaces);

router
    .route('/space/user/:idUser')
    .get(passportController.verifyToken, spaceController.searchSpaceByIdUser);

router
    .route('/space/:id')
    .get(passportController.verifyToken, spaceController.searchSpaceById)
    .delete(passportController.verifyToken, spaceController.deleteSpace)
    .patch(passportController.verifyToken, fileController.uploadFile('file'),
        fileController.uploadToCloudinary, spaceController.updateSpace)
    .put(passportController.verifyToken, spaceController.overwriteSpace);


/**
 * Petition routes
 */
router
    .route('/petition')
    .post(passportController.verifyToken, fileController.uploadFile('file'),
        fileController.uploadToCloudinary, petitionController.createPetition)
    .get(passportController.verifyToken, petitionController.listPetitions);

router
    .route('/petition/user/:idUser')
    .get(passportController.verifyToken,
        petitionController.searchPetitionByIdUser);

router
    .route('/petition/guardian/:idGuardian')
    .get(passportController.verifyToken,
        petitionController.searchPetitionByIdGuardian);

router
    .route('/petition/:id')
    .get(passportController.verifyToken, petitionController.searchPetitionById)
    .delete(passportController.verifyToken, petitionController.deletePetition)
    .patch(passportController.verifyToken, petitionController.updatePetition)
    .put(passportController.verifyToken, petitionController.overwritePetition);


/**
 * Message routes
 */
router
    .route('/message')
    .post(passportController.verifyToken, messageController.createMessage)
    .get(passportController.verifyToken, messageController.listMessages);

router
    .route('/message/:id')
    .get(passportController.verifyToken, messageController.searchMessageById)
    .delete(passportController.verifyToken, messageController.deleteMessage)
    .patch(passportController.verifyToken, messageController.updateMessage)
    .put(passportController.verifyToken, messageController.overwriteMessage);

router
    .route('/message/user-send/:id')
    .get(passportController.verifyToken,
        messageController.searchMessageByIdUserSend);

router
    .route('/message/user-receive/:id')
    .get(passportController.verifyToken,
        messageController.searchMessageByIdUserReceive);

module.exports = router;
